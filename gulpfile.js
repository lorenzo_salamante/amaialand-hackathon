var gulp = require('gulp');
var swPrecache = require('sw-precache');

gulp.task('generate-sw', function() {
  var swOptions = {
    staticFileGlobs: [
      './index.html',
      './images/*.{png,svg,gif,jpg}',
      './scripts/*.js',
      './styles/*.css'
    ],
    stripPrefix: '.',
    runtimeCaching: [{
      urlPattern: /^https:\/\/betaprojex\.com\/amaia_new\/api\/amaia\.php\?action\=getAllCondo/,
      handler: 'networkFirst',
      options: {
        cache: {
          name: 'condo-data'
        }
      }
    }]
  };
  return swPrecache.write('./service-worker.js', swOptions);
});

gulp.task('serve', ['generate-sw'], function() {
  // gulp.watch('./styles/*.scss', ['sass']);
  // browserSync({
  //   notify: false,
  //   logPrefix: 'weatherPWA',
  //   server: ['.'],
  //   open: false
  // });
  gulp.watch([
    './*.html',
    './scripts/*.js',
    './styles/*.css',
    '!./service-worker.js',
    '!./gulpfile.js'
  ], ['generate-sw']);
});

gulp.task('default', ['serve']);
