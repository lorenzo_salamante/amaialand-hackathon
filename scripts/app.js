(function() {
	'use strict';
	var xhr = new XMLHttpRequest();

	function getQueryVariable(variable) {
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i=0;i<vars.length;i++) {
			var pair = vars[i].split("=");
			if (pair[0] == variable) {
				return pair[1];
			}
		}
	}

	

	function getCondo()
	{
		var param = getQueryVariable("id");

		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4 && xhr.status === 200) {
				var response = JSON.parse(xhr.responseText);
				document.getElementById('condo-title').innerHTML = response.condo_title;
				document.getElementById('price-range').innerHTML = response.price_range;
				document.getElementById('lowest-monthly-amortization').innerHTML = response.lowest_monthly_amortization;
				document.getElementById('type-location').innerHTML = response.type_location;
				document.getElementById('overview-description').innerHTML = response.overview_description;
				document.getElementById('project-image').innerHTML = '<amp-img src="'+response.project_image+'" width="560" height="326" layout="responsive" alt="Amaia Steps Alabang"></amp-img>';
				document.getElementById('schema-address').innerHTML = response.schema_address;
				document.getElementById('location-description').innerHTML = response.location_description;
				document.getElementById('location-image').innerHTML = '<amp-img src="images/amaia-steps-vicinity-map.jpg" width="1050" height="735" layout="responsive" alt="Vicinity Map"></amp-img>';
				console.log(response);
			}
		};
		xhr.open('GET', 'https://betaprojex.com/amaia_new/api/amaia.php?action=getCondo&id='+param);
		xhr.send();
	}

	getCondo();
	'use strict';

	if ('serviceWorker' in navigator) {
		navigator.serviceWorker
		.register('/service-worker.js')
		.then(function() {
			console.log('Service Worker Registered');
		});
	}

})();
