# README #

### API ###

## Get All Condominiums ##

url: http://www.amaialand.com/api/amaia.php?action=getAllCondo

```json
[
    {
        "id": 162,
        "permalink": "http://www.amaialand.com/condominium/manila/amaia-skies-avenida-north-tower/",
        "title": "Amaia Skies Avenida North Tower",
        "tagline": "A quality investment awaits you",
        "short_description": "<p>With its central location at D. Jose St., Sta. Cruz, Manila, Amaia Skies Avenida North Tower offers you to feel and experience the highlights of Manila. Start your career on the right path too since transportation hubs are just around the corner from LRT 1 and LRT 3 to various public transports. You&#8217;ll be on time all the time.</p>\n",
        "price_range": "P1.4M - P2.9M",
        "unit_sizes": "18 sqm to 32.7 sqm",
        "list_image": "http://www.amaialand.com/wp-content/uploads/2017/04/skies-avenida-condo-list-img.jpg"
    },
    {
        "id": 173,
        "permalink": "http://www.amaialand.com/condominium/quezon-city/amaia-skies-cubao/",
        "title": "Amaia Skies Cubao",
        "tagline": "Reach your dream Life Now",
        "short_description": "<p>In Cubao, Quezon City rose a three-tower development called Amaia Skies Cubao. A first in the foray of Amaia&#8217;s high-rise condominium developments, this property is located in EDSA corner P. Tuazon St. High accessibility is key, and you can achieve it here. Cubao is the terminal hub with transport features such as MRT and bus stations. Live a dynamic lifestyle as all you need is just a stone&#8217;s throw away. The tower&#8217;s ground floor has retail shops while the podium level includes all the amenities you&#8217;ll need.</p>\n",
        "price_range": "P1.4M - P2.9M",
        "unit_sizes": "18 sqm - 34 sqm",
        "list_image": "http://www.amaialand.com/wp-content/uploads/2017/04/skies-cubao-condo-list-img.jpg"
    },
    {
        "id": 156,
        "permalink": "http://www.amaialand.com/condominium/mandaluyong-city/amaia-skies-shaw-north-tower/",
        "title": "Amaia Skies Shaw North Tower",
        "tagline": "Reach your dream Life Now",
        "short_description": "<p>Living in Amaia Skies Shaw North Tower is your first step to getting the career you have always dreamt of. Strategically located on Shaw Blvd. corner Samat Street in Brgy. Highway Hills, Mandaluyong City, you&#8217;ll love its proximity to business districts of Ortigas and Makati, which are just a bus or train ride away. It&#8217;s also close to commercial hubs as well as public transport systems like the MRT and shuttle services. Going to and fro your work is now easier!</p>\n",
        "price_range": "P1.6M - P2.9M",
        "unit_sizes": "18.6 sqm - 36.2 sqm",
        "list_image": "http://www.amaialand.com/wp-content/uploads/2017/04/shaw-condo-list-img.jpg"
    },
    {
        "id": 164,
        "permalink": "http://www.amaialand.com/condominium/manila/amaia-skies-sta-mesa-south-tower/",
        "title": "Amaia Skies Sta. Mesa South Tower",
        "tagline": "Reach your dream Life Now",
        "short_description": "<p>Amaia Skies Sta. Mesa South Tower takes condo living into a new height. Bask on the panoramic view of Manila&#8217;s skyline especially at night from your unit. The project is located in Valenzuela St. corner V. Mapa Blvd in Sta. Mesa Manila. What makes this location even more perfect is accessibility. From here, you can go to Pasig, Mandaluyong, Makati and Quezon City through extensive major road networks and highways. PUVs are available 24/7 as well.</p>\n",
        "price_range": "P1.5M - P2.9M",
        "unit_sizes": "18.6 sqm to 32 sqm",
        "list_image": "http://www.amaialand.com/wp-content/uploads/2017/04/skies-stamesa-condo-list-img.jpg"
    },...
]
```

## Get Single Condo ##

url: http://www.amaialand.com/api/amaia.php?action=getCondo&id=162

```json
{
    "": {
        "id": "162",
        "permalink": "http://www.amaialand.com/condominium/manila/amaia-skies-avenida-north-tower/",
        "title": "Amaia Skies Avenida North Tower",
        "tagline": "A quality investment awaits you",
        "short_description": "<p>With its central location at D. Jose St., Sta. Cruz, Manila, Amaia Skies Avenida North Tower offers you to feel and experience the highlights of Manila. Start your career on the right path too since transportation hubs are just around the corner from LRT 1 and LRT 3 to various public transports. You&#8217;ll be on time all the time.</p>\n",
        "price_range": "P1.4M - P2.9M",
        "unit_sizes": "18 sqm to 32.7 sqm",
        "list_image": "http://www.amaialand.com/wp-content/uploads/2017/04/skies-avenida-condo-list-img.jpg"
    }
}
```